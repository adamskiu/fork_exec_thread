#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

void * my_exec(void *a);

int main(int argc, char **argv)
{
  pthread_t tid;
  int result = 0;

  result = pthread_create(&tid, 0, my_exec, 0);
  if (result){
    perror("pthread_create: ");
    exit(1);
  }
  pthread_join(tid, 0);

  return 0;
}

void * my_exec(void *a)
{
  int result;
  char **argv = (char**) malloc(2 * sizeof(char*));
  argv[0] = (char*) malloc(7 * sizeof(char));
  argv[0] = "exec_p";
  argv[1] = (char*) malloc(2 * sizeof(char));
  argv[1] = "\0";

  result = execvp("./mock", argv);
  if (result == -1){
    perror("execvp:");
    exit(2);
  }
  return 0;
}
