#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

void * my_fork(void *a);

int main(int argc, char **argv)
{
  pthread_t tid;
  int result = 0;

  result = pthread_create(&tid, 0, my_fork, 0);
  if (result){
    perror("pthread_create: ");
    exit(1);
  }
  pthread_join(tid, 0);

  return 0;
}

void * my_fork(void *a)
{
  pid_t child_pid;
  child_pid = fork();
  if (child_pid == -1){
    perror("fork: ");
    exit(2);
  }
  if (child_pid) // rodzic
    ;
  else{   //dziecko
    printf("O borze, nie umieram :O\n" );
  }
  return 0;
}
