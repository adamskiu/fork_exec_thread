TARGET_FORK = fork_p
TARGET_EXEC = exec_p
TARGET_MOCK = mock
CC = gcc
CFLAGS = -Wall -Werror -g
LDFLAGS = -pthread
SOURCE_FORK = fork_main.c 
SOURCE_EXEC = exec_main.c

all:	$(TARGET_FORK) $(TARGET_EXEC) $(TARGET_MOCK)

$(TARGET_FORK):	
	$(CC) $(SOURCE_FORK) $(CFLAGS) $(LDFLAGS) -o $(TARGET_FORK)

$(TARGET_EXEC): 
	$(CC) $(SOURCE_EXEC) $(CFLAGS) $(LDFLAGS) -o $(TARGET_EXEC)

$(TARGET_MOCK):
	$(CC) mock.c $(CFLAGS) -o $(TARGET_MOCK)


clean:
	rm $(TARGET_FORK) $(TARGET_EXEC) $(TARGET_MOCK)
